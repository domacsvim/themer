local themes = {}

themes["onedark"] = {
  base00 = "#1b1f27",
  base01 = "#1e222a",
  base02 = "#282c34",
  base03 = "#353b45",
  base04 = "#3e4451",
  base05 = "#545862",
  base06 = "#b6bdca",
  base07 = "#c8ccd4",
  base08 = "#e06c75",
  base09 = "#d19a66",
  base0A = "#e5c07b",
  base0B = "#98c379",
  base0C = "#56b6c2",
  base0D = "#61afef",
  base0E = "#c678dd",
  base0F = "#be5046",
}

return themes
